#!/usr/bin/env bash
#
# Created:  Sat 12 Oct 2013 09:33:39 pm CEST
# Modified: Sat 12 Oct 2013 09:40:13 pm CEST CEST CEST CEST CEST
# Author:   Bert Van Vreckem <bert.vanvreckem@gmail.com>
#
# Download and unpack the latest Wordpress to the specified directory 

set -e # abort on nonzero exitstatus
set -u # abort on unbound variable

#--------------------------------------------------------------------------
# Functions
#--------------------------------------------------------------------------

usage() {
cat << _EOF_
Usage: ${0} DIR
  Downloads and unpacks the latest Wordpress into the specified directory
  DIR
_EOF_
}

#--------------------------------------------------------------------------
# Command line parsing
#--------------------------------------------------------------------------

if [[ "$#" -ne "1" ]]; then
    echo "Expected 1 argument (a directory), got $#" >&2
    usage
    exit 2
fi

#--------------------------------------------------------------------------
# Script proper
#--------------------------------------------------------------------------

if [ ! -d "${1}" ]; then
    echo "Expected a directory"
    usage
    exit 1
fi

# Remove blog directory from previous
if [ -d "${1}/blog/" ]; then
    rm -rf "${1}/blog/"
fi


wget http://wordpress.org/latest.zip 
unzip latest.zip -d "${1}"
mv "${1}/wordpress/" "${1}/blog/"
rm latest.zip

