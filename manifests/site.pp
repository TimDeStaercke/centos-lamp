 node default {
		class { 'apache': }
		class { 'apache::mod::ssl': }
        class { 'apache::mod::php': }
        class { 'mysql::server': }
        class { 'mysql::bindings':
                php_enable => true,
        }


       mysql::db { 'mydb':
      user     => 'myuser',
      password => 'mypass',
      host     => 'localhost',
    }
}